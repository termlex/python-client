# Refset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**copyright** | **str** |  | [optional] 
**created_time** | **datetime** |  | [optional] 
**date** | **datetime** |  | [optional] 
**definitions** | [**list[QueryExpression]**](QueryExpression.md) |  | [optional] 
**description** | **str** |  | [optional] 
**experimental** | **bool** |  | [optional] 
**extensible** | **bool** |  | [optional] 
**id** | **str** |  | [optional] 
**members** | **list[str]** |  | [optional] 
**module_id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**owner_id** | **str** |  | [optional] 
**publisher** | [**Organisation**](Organisation.md) |  | [optional] 
**sct_id** | **int** |  | [optional] 
**short_name** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**updated_by** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



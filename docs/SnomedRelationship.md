# SnomedRelationship

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** |  | [optional] 
**characteristic_type** | **str** |  | [optional] 
**code_system** | [**CodeSystem**](CodeSystem.md) |  | [optional] 
**created_time** | **datetime** |  | [optional] 
**defining_relation** | **bool** |  | [optional] 
**effective_time** | [**Calendar**](Calendar.md) |  | [optional] 
**id** | **str** |  | [optional] 
**inactivated_time** | **datetime** |  | [optional] 
**last_updated_time** | **datetime** |  | [optional] 
**mandatory** | **bool** |  | [optional] 
**module_id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**optional** | **bool** |  | [optional] 
**qualifying_relation** | **bool** |  | [optional] 
**refinability** | **str** |  | [optional] 
**refinable** | **bool** |  | [optional] 
**relationship_group** | **str** |  | [optional] 
**relationship_type** | **str** |  | [optional] 
**source** | **str** |  | [optional] 
**source_id** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**target_id** | **str** |  | [optional] 
**uuid** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



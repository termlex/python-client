# MetaData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accepted_date** | **datetime** |  | [optional] 
**accrual_methods** | **list[str]** |  | [optional] 
**accrual_periodicities** | **list[str]** |  | [optional] 
**accrual_policies** | **list[str]** |  | [optional] 
**audience_education_levels** | **list[str]** |  | [optional] 
**audience_mediators** | **list[str]** |  | [optional] 
**available_date** | **datetime** |  | [optional] 
**contributors** | [**list[Person]**](Person.md) |  | [optional] 
**copyrighted_date** | **datetime** |  | [optional] 
**created_date** | **datetime** |  | [optional] 
**creators** | [**list[Person]**](Person.md) |  | [optional] 
**description** | **str** |  | [optional] 
**format** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**identifier** | [**URI**](URI.md) |  | [optional] 
**instructional_methods** | **list[str]** |  | [optional] 
**issued_date** | **datetime** |  | [optional] 
**language** | **str** |  | [optional] 
**last_updated_date** | **datetime** |  | [optional] 
**licenses** | [**list[URI]**](URI.md) |  | [optional] 
**modified_dates** | **list[datetime]** |  | [optional] 
**provenances** | **list[str]** |  | [optional] 
**publishers** | [**list[Person]**](Person.md) |  | [optional] 
**replaced_date** | **datetime** |  | [optional] 
**retired_date** | **datetime** |  | [optional] 
**rights_holders** | [**list[Person]**](Person.md) |  | [optional] 
**sources** | [**list[URI]**](URI.md) |  | [optional] 
**subject** | **str** |  | [optional] 
**submitted_date** | **datetime** |  | [optional] 
**temporal_coverage** | [**CollectionOfCalendar**](CollectionOfCalendar.md) |  | [optional] 
**title** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**valid_date** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



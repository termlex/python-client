# SnomedDescription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** |  | [optional] 
**case_significance_type** | **str** |  | [optional] 
**concept_id** | **str** |  | [optional] 
**effective_time** | [**Calendar**](Calendar.md) |  | [optional] 
**id** | **str** |  | [optional] 
**initial_cap_status** | **bool** |  | [optional] 
**language** | **str** |  | [optional] 
**module_id** | **int** |  | [optional] 
**source** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**term** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**uuid** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# TerminologyConcept

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** |  | [optional] 
**active_concept** | **bool** |  | [optional] 
**code_system** | [**CodeSystem**](CodeSystem.md) |  | [optional] 
**created_time** | **datetime** |  | [optional] 
**fully_specified_name** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**inactivated_time** | **datetime** |  | [optional] 
**last_updated_time** | **datetime** |  | [optional] 
**preferred_term** | **str** |  | [optional] 
**source** | **str** |  | [optional] 
**synonyms** | [**CollectionOfstring**](CollectionOfstring.md) |  | [optional] 
**type** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# swagger_client.HierarchyresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_ancestor_count_for_id_using_get**](HierarchyresourceApi.md#get_ancestor_count_for_id_using_get) | **GET** /api/hierarchy/{id}/counts/ancestors | get the ancestor count for concept with given id
[**get_ancestors_for_id_async_using_get**](HierarchyresourceApi.md#get_ancestors_for_id_async_using_get) | **GET** /api/hierarchy/version/{version}/id/{id}/anc | get the ancestors of concept with given id
[**get_ancestors_for_id_using_get**](HierarchyresourceApi.md#get_ancestors_for_id_using_get) | **GET** /api/hierarchy/version/{version}/id/{id}/ancestors | get the ancestors of concept with given id
[**get_children_count_for_id_using_get**](HierarchyresourceApi.md#get_children_count_for_id_using_get) | **GET** /api/hierarchy/version/{version}/id/{id}/counts/children | get the children count for concept with given id
[**get_children_for_id_using_get**](HierarchyresourceApi.md#get_children_for_id_using_get) | **GET** /api/hierarchy/version/{version}/id/{id}/children | get the children of concept with given id
[**get_counts_for_id_using_get**](HierarchyresourceApi.md#get_counts_for_id_using_get) | **GET** /api/hierarchy/version/{version}/id/{id}/counts | get an aggregate count of children, parent, ancestor and descendant counts for concept with given id
[**get_descendant_count_for_id_using_get**](HierarchyresourceApi.md#get_descendant_count_for_id_using_get) | **GET** /api/hierarchy/version/{version}/id/{id}/counts/descendants | gets the descendant count for concept with given id
[**get_descendants_for_id_async_using_get**](HierarchyresourceApi.md#get_descendants_for_id_async_using_get) | **GET** /api/hierarchy/version/{version}/id/{id}/desc | get the descendants of concept with given id
[**get_descendants_for_id_using_get**](HierarchyresourceApi.md#get_descendants_for_id_using_get) | **GET** /api/hierarchy/version/{version}/id/{id}/descendants | get the descendants of concept with given id
[**get_parent_count_for_id_using_get**](HierarchyresourceApi.md#get_parent_count_for_id_using_get) | **GET** /api/hierarchy/{id}/counts/parents | get the parent count for concept with given id
[**get_parents_for_id_using_get**](HierarchyresourceApi.md#get_parents_for_id_using_get) | **GET** /api/hierarchy/version/{version}/id/{id}/parents | get the parents of concept with given id
[**get_proximal_primitive_parents_for_id_using_get**](HierarchyresourceApi.md#get_proximal_primitive_parents_for_id_using_get) | **GET** /api/hierarchy/version/{version}/id/{id}/proximalparents | get the proximal primitive parents for concept with given id


# **get_ancestor_count_for_id_using_get**
> int get_ancestor_count_for_id_using_get(id, version)

get the ancestor count for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HierarchyresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the ancestor count for concept with given id
    api_response = api_instance.get_ancestor_count_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling HierarchyresourceApi->get_ancestor_count_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ancestors_for_id_async_using_get**
> list[object] get_ancestors_for_id_async_using_get(id, version, definition_status=definition_status)

get the ancestors of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HierarchyresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string
definition_status = 'definition_status_example' # str | definitionStatus (optional)

try: 
    # get the ancestors of concept with given id
    api_response = api_instance.get_ancestors_for_id_async_using_get(id, version, definition_status=definition_status)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling HierarchyresourceApi->get_ancestors_for_id_async_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 
 **definition_status** | **str**| definitionStatus | [optional] 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ancestors_for_id_using_get**
> list[object] get_ancestors_for_id_using_get(id, version, definition_status=definition_status)

get the ancestors of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HierarchyresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string
definition_status = 'definition_status_example' # str | definitionStatus (optional)

try: 
    # get the ancestors of concept with given id
    api_response = api_instance.get_ancestors_for_id_using_get(id, version, definition_status=definition_status)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling HierarchyresourceApi->get_ancestors_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 
 **definition_status** | **str**| definitionStatus | [optional] 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_children_count_for_id_using_get**
> int get_children_count_for_id_using_get(id, version)

get the children count for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HierarchyresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the children count for concept with given id
    api_response = api_instance.get_children_count_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling HierarchyresourceApi->get_children_count_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_children_for_id_using_get**
> list[object] get_children_for_id_using_get(id, version)

get the children of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HierarchyresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the children of concept with given id
    api_response = api_instance.get_children_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling HierarchyresourceApi->get_children_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_counts_for_id_using_get**
> object get_counts_for_id_using_get(id, version)

get an aggregate count of children, parent, ancestor and descendant counts for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HierarchyresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get an aggregate count of children, parent, ancestor and descendant counts for concept with given id
    api_response = api_instance.get_counts_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling HierarchyresourceApi->get_counts_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_descendant_count_for_id_using_get**
> int get_descendant_count_for_id_using_get(id, version)

gets the descendant count for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HierarchyresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # gets the descendant count for concept with given id
    api_response = api_instance.get_descendant_count_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling HierarchyresourceApi->get_descendant_count_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_descendants_for_id_async_using_get**
> list[object] get_descendants_for_id_async_using_get(id, version)

get the descendants of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HierarchyresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the descendants of concept with given id
    api_response = api_instance.get_descendants_for_id_async_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling HierarchyresourceApi->get_descendants_for_id_async_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_descendants_for_id_using_get**
> list[object] get_descendants_for_id_using_get(id, version)

get the descendants of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HierarchyresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the descendants of concept with given id
    api_response = api_instance.get_descendants_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling HierarchyresourceApi->get_descendants_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_parent_count_for_id_using_get**
> int get_parent_count_for_id_using_get(id, version)

get the parent count for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HierarchyresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the parent count for concept with given id
    api_response = api_instance.get_parent_count_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling HierarchyresourceApi->get_parent_count_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_parents_for_id_using_get**
> list[object] get_parents_for_id_using_get(id, version, definition_status=definition_status)

get the parents of concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HierarchyresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string
definition_status = 'definition_status_example' # str | definitionStatus (optional)

try: 
    # get the parents of concept with given id
    api_response = api_instance.get_parents_for_id_using_get(id, version, definition_status=definition_status)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling HierarchyresourceApi->get_parents_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 
 **definition_status** | **str**| definitionStatus | [optional] 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_proximal_primitive_parents_for_id_using_get**
> list[object] get_proximal_primitive_parents_for_id_using_get(id, version)

get the proximal primitive parents for concept with given id

A 404 error is thrown when no matching concept is found for id. When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.HierarchyresourceApi()
id = 'id_example' # str | id
version = 'version_example' # str | The version of the terminology product as date string

try: 
    # get the proximal primitive parents for concept with given id
    api_response = api_instance.get_proximal_primitive_parents_for_id_using_get(id, version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling HierarchyresourceApi->get_proximal_primitive_parents_for_id_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| id | 
 **version** | **str**| The version of the terminology product as date string | 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


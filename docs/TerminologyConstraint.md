# TerminologyConstraint

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cgf** | **str** |  | [optional] 
**dimension_vocabulary** | **str** |  | [optional] 
**expression** | [**Expression**](Expression.md) |  | [optional] 
**expression_uuid** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**precoordinated** | **bool** |  | [optional] 
**subsumption** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**uuid** | **str** |  | [optional] 
**value** | [**ConstraintValueOfobject**](ConstraintValueOfobject.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



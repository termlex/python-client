# NormalFormExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**child_expressions** | [**CollectionOfExpression**](CollectionOfExpression.md) |  | [optional] 
**compositional_grammar_form** | **str** |  | [optional] 
**equivalent_expressions** | [**CollectionOfExpression**](CollectionOfExpression.md) |  | [optional] 
**focus_concepts** | [**CollectionOfSnomedConcept**](CollectionOfSnomedConcept.md) |  | [optional] 
**id** | **str** |  | [optional] 
**parent_expressions** | [**CollectionOfExpression**](CollectionOfExpression.md) |  | [optional] 
**proximal_primitives** | [**CollectionOfSnomedConcept**](CollectionOfSnomedConcept.md) |  | [optional] 
**relationship_expressions** | [**CollectionOfSnomedRelationshipPropertyExpression**](CollectionOfSnomedRelationshipPropertyExpression.md) |  | [optional] 
**role_group_expressions** | [**CollectionOfSnomedRoleGroupExpression**](CollectionOfSnomedRoleGroupExpression.md) |  | [optional] 
**singleton_focus_concept** | [**SnomedConcept**](SnomedConcept.md) |  | [optional] 
**singleton_focus_concept_type** | **str** |  | [optional] 
**uuid** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



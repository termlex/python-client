# swagger_client.SearchcontrollerApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**search_refsets_using_get**](SearchcontrollerApi.md#search_refsets_using_get) | **GET** /api/search/refsets | Returns matches for given token for a specified version of SNOMED CT as a list of map objects
[**search_snomed_using_get**](SearchcontrollerApi.md#search_snomed_using_get) | **GET** /api/search/sct | Returns matches for given token for a specified version of SNOMED CT as a list of map objects


# **search_refsets_using_get**
> object search_refsets_using_get(term, included_refset_ids, max_results_size=max_results_size, start=start, concept_types=concept_types, include_fs_ns=include_fs_ns, include_inactive=include_inactive, edition=edition, version=version, included_members=included_members, excluded_members=excluded_members, excluded_refset_ids=excluded_refset_ids, language_code=language_code)

Returns matches for given token for a specified version of SNOMED CT as a list of map objects

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.SearchcontrollerApi()
term = 'term_example' # str | Token to search for; can be a term or an id
included_refset_ids = ['included_refset_ids_example'] # list[str] | The ids of refsets used to limit the search results
max_results_size = 100 # int | Maximum number of results to return (optional) (default to 100)
start = 0 # int | The start position of search results to return (optional) (default to 0)
concept_types = ['concept_types_example'] # list[str] | The concept hierarchies/types to filter results to (optional)
include_fs_ns = false # bool | Toggle inclusion of FSNs in results (optional) (default to false)
include_inactive = false # bool | Toggle inclusion of inactive concepts in results (optional) (default to false)
edition = 'int' # str | The nam of the SNOMED CT edition to search against (optional) (default to int)
version = 'version_example' # str | The version of the terminology product as date string. Defaults to default version specified in server instance (optional)
included_members = ['included_members_example'] # list[str] | The concepts to include in search results (optional)
excluded_members = ['excluded_members_example'] # list[str] | The concepts to exclude from search results (optional)
excluded_refset_ids = ['excluded_refset_ids_example'] # list[str] | The ids of refsets to exclude from search results (optional)
language_code = 'en' # str | The language of descriptions to search (optional) (default to en)

try: 
    # Returns matches for given token for a specified version of SNOMED CT as a list of map objects
    api_response = api_instance.search_refsets_using_get(term, included_refset_ids, max_results_size=max_results_size, start=start, concept_types=concept_types, include_fs_ns=include_fs_ns, include_inactive=include_inactive, edition=edition, version=version, included_members=included_members, excluded_members=excluded_members, excluded_refset_ids=excluded_refset_ids, language_code=language_code)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling SearchcontrollerApi->search_refsets_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **term** | **str**| Token to search for; can be a term or an id | 
 **included_refset_ids** | [**list[str]**](str.md)| The ids of refsets used to limit the search results | 
 **max_results_size** | **int**| Maximum number of results to return | [optional] [default to 100]
 **start** | **int**| The start position of search results to return | [optional] [default to 0]
 **concept_types** | [**list[str]**](str.md)| The concept hierarchies/types to filter results to | [optional] 
 **include_fs_ns** | **bool**| Toggle inclusion of FSNs in results | [optional] [default to false]
 **include_inactive** | **bool**| Toggle inclusion of inactive concepts in results | [optional] [default to false]
 **edition** | **str**| The nam of the SNOMED CT edition to search against | [optional] [default to int]
 **version** | **str**| The version of the terminology product as date string. Defaults to default version specified in server instance | [optional] 
 **included_members** | [**list[str]**](str.md)| The concepts to include in search results | [optional] 
 **excluded_members** | [**list[str]**](str.md)| The concepts to exclude from search results | [optional] 
 **excluded_refset_ids** | [**list[str]**](str.md)| The ids of refsets to exclude from search results | [optional] 
 **language_code** | **str**| The language of descriptions to search | [optional] [default to en]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **search_snomed_using_get**
> object search_snomed_using_get(term, max_results_size=max_results_size, start=start, concept_types=concept_types, include_fs_ns=include_fs_ns, include_inactive=include_inactive, edition=edition, version=version, included_members=included_members, excluded_members=excluded_members, excluded_refset_ids=excluded_refset_ids, language_code=language_code)

Returns matches for given token for a specified version of SNOMED CT as a list of map objects

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.SearchcontrollerApi()
term = 'term_example' # str | Token to search for; can be a term or an id
max_results_size = 100 # int | Maximum number of results to return (optional) (default to 100)
start = 0 # int | The start position of search results to return (optional) (default to 0)
concept_types = ['concept_types_example'] # list[str] | The concept hierarchies/types to filter results to (optional)
include_fs_ns = false # bool | Toggle inclusion of FSNs in results (optional) (default to false)
include_inactive = false # bool | Toggle inclusion of inactive concepts in results (optional) (default to false)
edition = 'int' # str | The name of the SNOMED CT edition to search against (optional) (default to int)
version = 'version_example' # str | The version of the terminology product as date string. Defaults to default version specified in server instance (optional)
included_members = ['included_members_example'] # list[str] | The concepts to include in search results (optional)
excluded_members = ['excluded_members_example'] # list[str] | The concepts to exclude from search results (optional)
excluded_refset_ids = ['excluded_refset_ids_example'] # list[str] | The ids of refsets to exclude from search results (optional)
language_code = 'en' # str | The language of descriptions to search (optional) (default to en)

try: 
    # Returns matches for given token for a specified version of SNOMED CT as a list of map objects
    api_response = api_instance.search_snomed_using_get(term, max_results_size=max_results_size, start=start, concept_types=concept_types, include_fs_ns=include_fs_ns, include_inactive=include_inactive, edition=edition, version=version, included_members=included_members, excluded_members=excluded_members, excluded_refset_ids=excluded_refset_ids, language_code=language_code)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling SearchcontrollerApi->search_snomed_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **term** | **str**| Token to search for; can be a term or an id | 
 **max_results_size** | **int**| Maximum number of results to return | [optional] [default to 100]
 **start** | **int**| The start position of search results to return | [optional] [default to 0]
 **concept_types** | [**list[str]**](str.md)| The concept hierarchies/types to filter results to | [optional] 
 **include_fs_ns** | **bool**| Toggle inclusion of FSNs in results | [optional] [default to false]
 **include_inactive** | **bool**| Toggle inclusion of inactive concepts in results | [optional] [default to false]
 **edition** | **str**| The name of the SNOMED CT edition to search against | [optional] [default to int]
 **version** | **str**| The version of the terminology product as date string. Defaults to default version specified in server instance | [optional] 
 **included_members** | [**list[str]**](str.md)| The concepts to include in search results | [optional] 
 **excluded_members** | [**list[str]**](str.md)| The concepts to exclude from search results | [optional] 
 **excluded_refset_ids** | [**list[str]**](str.md)| The ids of refsets to exclude from search results | [optional] 
 **language_code** | **str**| The language of descriptions to search | [optional] [default to en]

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


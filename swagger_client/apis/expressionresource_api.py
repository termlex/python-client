# coding: utf-8

"""
    Termlex API

    Termlex is a RESTful, service oriented terminology server and SNOMED CT API

    OpenAPI spec version: 3.2.0
    Contact: info@noesisinformatica.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..configuration import Configuration
from ..api_client import ApiClient


class ExpressionresourceApi(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        config = Configuration()
        if api_client:
            self.api_client = api_client
        else:
            if not config.api_client:
                config.api_client = ApiClient()
            self.api_client = config.api_client

    def get_compositional_grammar_form_using_get(self, **kwargs):
        """
        get the compositional grammar form of expression
        When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_compositional_grammar_form_using_get(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str expression: The expression to be rendered
        :return: str
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.get_compositional_grammar_form_using_get_with_http_info(**kwargs)
        else:
            (data) = self.get_compositional_grammar_form_using_get_with_http_info(**kwargs)
            return data

    def get_compositional_grammar_form_using_get_with_http_info(self, **kwargs):
        """
        get the compositional grammar form of expression
        When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_compositional_grammar_form_using_get_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str expression: The expression to be rendered
        :return: str
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['expression']
        all_params.append('callback')
        all_params.append('_return_http_data_only')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_compositional_grammar_form_using_get" % key
                )
            params[key] = val
        del params['kwargs']

        resource_path = '/api/expressions/cgf'.replace('{format}', 'json')
        path_params = {}

        query_params = {}
        if 'expression' in params:
            query_params['expression'] = params['expression']

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None

        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['*/*'])
        if not header_params['Accept']:
            del header_params['Accept']

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api(resource_path, 'GET',
                                            path_params,
                                            query_params,
                                            header_params,
                                            body=body_params,
                                            post_params=form_params,
                                            files=local_var_files,
                                            response_type='str',
                                            auth_settings=auth_settings,
                                            callback=params.get('callback'),
                                            _return_http_data_only=params.get('_return_http_data_only'))

    def get_long_normal_form_using_get(self, **kwargs):
        """
        get the long normal form of expression
        When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_long_normal_form_using_get(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str expression: The expression to be rendered
        :param str version: The version of the terminology product as date string
        :return: NormalFormExpression
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.get_long_normal_form_using_get_with_http_info(**kwargs)
        else:
            (data) = self.get_long_normal_form_using_get_with_http_info(**kwargs)
            return data

    def get_long_normal_form_using_get_with_http_info(self, **kwargs):
        """
        get the long normal form of expression
        When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_long_normal_form_using_get_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str expression: The expression to be rendered
        :param str version: The version of the terminology product as date string
        :return: NormalFormExpression
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['expression', 'version']
        all_params.append('callback')
        all_params.append('_return_http_data_only')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_long_normal_form_using_get" % key
                )
            params[key] = val
        del params['kwargs']

        resource_path = '/api/expressions/lnf'.replace('{format}', 'json')
        path_params = {}

        query_params = {}
        if 'expression' in params:
            query_params['expression'] = params['expression']
        if 'version' in params:
            query_params['version'] = params['version']

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None

        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['*/*'])
        if not header_params['Accept']:
            del header_params['Accept']

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api(resource_path, 'GET',
                                            path_params,
                                            query_params,
                                            header_params,
                                            body=body_params,
                                            post_params=form_params,
                                            files=local_var_files,
                                            response_type='NormalFormExpression',
                                            auth_settings=auth_settings,
                                            callback=params.get('callback'),
                                            _return_http_data_only=params.get('_return_http_data_only'))

    def get_short_normal_form_using_get(self, **kwargs):
        """
        get the long normal form of expression
        When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_short_normal_form_using_get(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str expression: The expression to be rendered
        :param str version: The version of the terminology product as date string
        :return: NormalFormExpression
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.get_short_normal_form_using_get_with_http_info(**kwargs)
        else:
            (data) = self.get_short_normal_form_using_get_with_http_info(**kwargs)
            return data

    def get_short_normal_form_using_get_with_http_info(self, **kwargs):
        """
        get the long normal form of expression
        When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_short_normal_form_using_get_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str expression: The expression to be rendered
        :param str version: The version of the terminology product as date string
        :return: NormalFormExpression
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['expression', 'version']
        all_params.append('callback')
        all_params.append('_return_http_data_only')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_short_normal_form_using_get" % key
                )
            params[key] = val
        del params['kwargs']

        resource_path = '/api/expressions/snf'.replace('{format}', 'json')
        path_params = {}

        query_params = {}
        if 'expression' in params:
            query_params['expression'] = params['expression']
        if 'version' in params:
            query_params['version'] = params['version']

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None

        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['*/*'])
        if not header_params['Accept']:
            del header_params['Accept']

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api(resource_path, 'GET',
                                            path_params,
                                            query_params,
                                            header_params,
                                            body=body_params,
                                            post_params=form_params,
                                            files=local_var_files,
                                            response_type='NormalFormExpression',
                                            auth_settings=auth_settings,
                                            callback=params.get('callback'),
                                            _return_http_data_only=params.get('_return_http_data_only'))

    def get_subsumption_relationship_using_get1(self, **kwargs):
        """
        get the subsumption relationship of comparing two expressions
        When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_subsumption_relationship_using_get1(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str expression1: The left hand side expression to be compared
        :param str expression2: The right hand side expression to be compared
        :param str version: The version of the terminology product as date string
        :return: str
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.get_subsumption_relationship_using_get1_with_http_info(**kwargs)
        else:
            (data) = self.get_subsumption_relationship_using_get1_with_http_info(**kwargs)
            return data

    def get_subsumption_relationship_using_get1_with_http_info(self, **kwargs):
        """
        get the subsumption relationship of comparing two expressions
        When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.get_subsumption_relationship_using_get1_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str expression1: The left hand side expression to be compared
        :param str expression2: The right hand side expression to be compared
        :param str version: The version of the terminology product as date string
        :return: str
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['expression1', 'expression2', 'version']
        all_params.append('callback')
        all_params.append('_return_http_data_only')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_subsumption_relationship_using_get1" % key
                )
            params[key] = val
        del params['kwargs']

        resource_path = '/api/expressions/compare'.replace('{format}', 'json')
        path_params = {}

        query_params = {}
        if 'expression1' in params:
            query_params['expression1'] = params['expression1']
        if 'expression2' in params:
            query_params['expression2'] = params['expression2']
        if 'version' in params:
            query_params['version'] = params['version']

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None

        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['*/*'])
        if not header_params['Accept']:
            del header_params['Accept']

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api(resource_path, 'GET',
                                            path_params,
                                            query_params,
                                            header_params,
                                            body=body_params,
                                            post_params=form_params,
                                            files=local_var_files,
                                            response_type='str',
                                            auth_settings=auth_settings,
                                            callback=params.get('callback'),
                                            _return_http_data_only=params.get('_return_http_data_only'))
